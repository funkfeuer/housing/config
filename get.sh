#!/bin/sh

. ./creds.sh
SSH_OPTS="-o ConnectTimeout=5 -o StrictHostKeyChecking=no -o ControlMaster=auto -o ControlPersist=1m -o ControlPath=/tmp/.%C"
GIT_OPTS="-c user.name='root' -c user.email='root@config'"
MQTT_OPTS="-h mqtt.funkfeuer.at -p 8883 --tls-version tlsv1.2 --cafile /etc/ssl/certs/ca-certificates.crt"
ANTENNAS="vkm-csph csph-vkm csph-nig nig-csph csph-krypta"
SWITCHES="sw-vault-rc sw-vault-r1a sw-vault-r1b sw-vault-r2 sw-vault-e sw-vault-ffa sw-vault-ffb"
EDGESWITCHES="sr05vkm"

commit()
{
	git add *.cfg
	git $GIT_OPTS commit -m "$1 `date +"%Y-%m-%d"`" >/dev/null
}

backup_mikrotik()
{
	CMD="/export terse;/user export terse"
	FILE=$1.cfg.tmp
	sshpass -p$PASS ssh $SSH_OPTS $USER@$1 $CMD > $FILE 2>/dev/null || { rm $FILE; return; }
	sed -i 7,\${/^#/d} $FILE
	sed -i '1s/^.*by//' $FILE
	dos2unix $FILE 2>/dev/null
	mv $FILE $1.cfg
	commit $1
}

for ANTENNA in $ANTENNAS; do
	CMD="interface w60g monitor wlan60-1 once"
	DATA=`sshpass -p$PASS ssh $SSH_OPTS $USER@$ANTENNA $CMD` 2>/dev/null || return
	DATE=\"Time\":\"`date +"%Y-%m-%dT%H:%M:%S"`\",
	JSON=`echo "$DATA" | sed -e '$d' | sed -e 's/[[:blank:]]*\([[:print:]]*\): \([[:print:]]*\)/"\1":"\2",/' -e "1s/^/{\n${DATE}\n/" -e '$s/,.$/\n}/'`
	# GNU sed -e 's/[[:blank:]]*\(.*\): \(.*\)/  "\1": "\2",/' -e "1s/^/{\n${DATE}\n/" -e '$s/,/\n}/'
	JSON=`echo "$JSON" | tr -d '\r\n'`
	DEV=ant_$ANTENNA
	mosquitto_pub $MQTT_OPTS -t "tele/$DEV/SENSOR" -u $DEV -P "$PASS" -m "$JSON"
	backup_mikrotik $ANTENNA
done

for SWITCH in $SWITCHES; do
	backup_mikrotik $SWITCH
done

for SWITCH in $EDGESWITCHES; do
	FILE=$SWITCH.cfg.tmp
	IP=$(getent hosts $SWITCH | awk '{ print $1 }')
	CFG=$(./get_edgeswitch_cfg.exp $IP $USER $PASS) || continue
	echo "$CFG" | sed 's/\r$//; /!System Up/d; /password/d' > $FILE
	mv $FILE $SWITCH.cfg
	commit $SWITCH
done

git push -q
